import { useEffect, useState } from "react";
import { connectSocket } from "../websocket/websocket_init";
import {
  CloudSyncOutlined,
  LoadingOutlined,
  CheckCircleOutlined,
} from "@ant-design/icons";
import { Spin } from "antd";

const antIcon = <LoadingOutlined style={{ fontSize: 24 }} spin />;

function TopBar({ setReconnect, wsConenction }) {
  const [loading, setLoading] = useState(false);
  const [active, setActive] = useState(false);

  useEffect(() => {
    if (loading) {
      setTimeout(function () {
        setLoading(false);
        setActive(true);
      }, 5000);
    }
  }, [loading]);


  return (
    <>
      <div
        id="topbar"
        style={{ background: !active ? "red" : "green" }}
        onClick={() => {
          setLoading(true);
          connectSocket();
        }}
      >
        {!loading ? (
          <>
            {!active ? <CloudSyncOutlined /> : <CheckCircleOutlined />}
            <div id="text">
              <h1>
                {!active
                  ? "Diconnected from internet. Please click to retry"
                  : "The Internet is reconnected"}
              </h1>
            </div>
          </>
        ) : (
          <Spin indicator={antIcon} />
        )}
      </div>
    </>
  );
}

export default TopBar;
