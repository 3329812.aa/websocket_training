import { useSelector ,shallowEqual } from "react-redux";

export default function Content() {
  const messages = useSelector(state => state.basic.messages, shallowEqual)
  return (
    <div className="contnent">
      <div>{messages.length ? messages : 'Your code here ...'}</div>
    </div>
  );
}
