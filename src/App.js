import "./App.css";
import SiderDemo from "./pages/main";
import { connectSocket } from "../src/websocket/websocket_init";
import { useEffect, useState } from "react";
import { useSelector } from "react-redux";
import { notification, Progress } from "antd";
import TopBar from "./pages/topbar";

function App() {
  const progress = useSelector((state) => state.basic.progress);
  const notificate = useSelector((state) => state.basic.notification);
  const wsConnection = useSelector((state) => state.basic.connection);

  const [reconnect, setReconnect] = useState(true);
  console.log("reconnect", reconnect);

  useEffect(() => {
    connectSocket();
  }, []);
  useEffect(() => {
    if (!wsConnection) {
      setReconnect(false);
    } else {
      setTimeout(function () {
        setReconnect(true);
      }, 6000);
    }
  }, [wsConnection]);
  useEffect(() => {
    notification.open({
      message: "PROGRESS",
      description: <Progress percent={progress} />,
      duration: null,
      key: "progres",
      top: 50,
    });
  }, [progress]);

  useEffect(() => {
    notification.open({
      message: "Notification",
      placement: "topLeft",
      description: notificate,
      top: 50,
    });
  }, [notificate]);
  return (
    <div className="App">
      {!reconnect && (
        <TopBar setReconnect={setReconnect} wsConnection={wsConnection} />
      )}
      <SiderDemo />
    </div>
  );
}

export default App;
