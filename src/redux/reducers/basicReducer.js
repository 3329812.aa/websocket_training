const initialState = {
  messages: [],
  progress: 0,
  notification: "",
  connection: false,
};

export default function basicReducer(state = initialState, action) {
  switch (action.type) {
    case "ADD":
      return { ...state, messages: action.payload };
    case "PROGRESS":
      return { ...state, progress: action.payload };
    case "NOTIFICATION":
      return { ...state, notification: action.payload };
    case "CONNECTION":
      return { ...state, connection: action.payload };
    default:
      return state;
  }
}
