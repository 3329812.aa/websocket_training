import { combineReducers, createStore } from "redux";


import basicReducer from "./reducers/basicReducer";

const reducers = combineReducers({
  basic: basicReducer,
});

export const store = createStore(reducers);
