import { store } from "../redux/store";


export const connectSocket = () => {
  let ws = new WebSocket("wss://ws.car24.uz/ws-random-2");

  ws.onopen = () => {
    console.log('socket connected')
    store.dispatch({
        type:"CONNECTION",
        payload: true
    })
  }
  ws.onmessage = function (event) {
    const json = JSON.parse(event.data);
    try {
      if (json.type === 'progress') {
          store.dispatch({
              type:"PROGRESS",
              payload: json.data.count
          })
      } else if(json.type === "notification") {
          store.dispatch({
              type: "NOTIFICATION",
              payload : json.data.description
          })
      } else if (json.type === 'message'){
          store.dispatch({
              type: "ADD",
              payload: json.data.description
          })
      }
    } catch (error) {
      console.log(error);
    }
  };

  ws.onclose = () => {
    console.log('closing connection')

    store.dispatch({
        type:"CONNECTION",
        payload: false
    })
    // auto reconnection when socket is unconnected
    // setTimeout(function() {
    //     connectSocket();
    //   }, 1000);
  }

  ws.onerror = function(err) {
    console.error(err)
    // ws.close();
  };
};
